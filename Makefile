CC	:= aarch64-linux-gnu-gcc
CFLAGS	+= -O3 -Wall -Werror

OBJS	= main.o
OBJS	+= memcpy_ldp_str.o
OBJS	+= memcpy_ldp_sttr.o
OBJS	+= memcpy_ldp_stp.o
OBJS	+= memcpy_ldr_stp.o
OBJS	+= memcpy_ldtr_stp.o

memcpy: $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $@

%.o: %.S
	$(CC) $(CFLAGS) -c $<

%.o: %.c
	$(CC) $(CFLAGS) -c $<

clean:
	rm -rf $(OBJS)
	rm -rf memcpy
