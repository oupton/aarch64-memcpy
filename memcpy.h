#ifndef __MEMCPY_H__
#define __MEMCPY_H__

#include <stdint.h>

void memcpy_ldp_str(void *dst, const void *src, size_t count);
void memcpy_ldp_sttr(void *dst, const void *src, size_t count);
void memcpy_ldr_stp(void *dst, const void *src, size_t count);
void memcpy_ldtr_stp(void *dst, const void *src, size_t count);
void memcpy_ldp_stp(void *dst, const void *src, size_t count);

#endif  /* __MEMCPY_H__ */
