#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

#include "memcpy.h"

#define KB	1024
#define MB	(1024 * KB)
#define GB	(1024 * MB)

enum mode {
	LDP_STR		= 0,
	LDP_STTR,
	LDR_STP,
	LDTR_STP,
	LDP_STP,

	NR_MODES
};

static size_t test_size = 2UL * GB;
static size_t test_iterations = 100;
static enum mode test_mode;

#define PR_MODE(mode)				\
	printf("    %d: "#mode"\n", mode);

static void pr_modes(void)
{
	PR_MODE(LDP_STR);
	PR_MODE(LDP_STTR);
	PR_MODE(LDR_STP);
	PR_MODE(LDTR_STP);
	PR_MODE(LDP_STP);
}

static void pr_help(const char *progname)
{
	printf("%s [OPTIONS]\n", progname);
	printf("  -s SIZE		(default: %lu)\n", test_size);
	printf("  -i ITERATIONS		(default: %lu)\n", test_iterations);
	printf("  -m MODE		(default: %d)\n", test_mode);
	pr_modes();
	printf("  -h prints this message\n");
}

static void *setup_test_buffer(void)
{
	void *buf = mmap(0, test_size, PROT_READ|PROT_WRITE,
			 MAP_PRIVATE|MAP_ANONYMOUS|MAP_HUGETLB, 0, 0);

	assert(buf != MAP_FAILED);
	memset(buf, 0xf, test_size);
	return buf;
}

static void destroy_test_buffer(void *buf)
{
	munmap(buf, test_size);
}

static void do_memcpy(void *dst, const void *src)
{
	switch (test_mode) {
	case LDP_STR:
		memcpy_ldp_str(dst, src, test_size);
		break;
	case LDP_STTR:
		memcpy_ldp_sttr(dst, src, test_size);
		break;
	case LDR_STP:
		memcpy_ldr_stp(dst, src, test_size);
		break;
	case LDTR_STP:
		memcpy_ldtr_stp(dst, src, test_size);
		break;
	case LDP_STP:
		memcpy_ldp_stp(dst, src, test_size);
		break;
	default:
		assert(0);
	}
}

static void run_test(void)
{
	void *dst = setup_test_buffer();
	void *src = setup_test_buffer();
	size_t i;

	printf("Iters:	%lu\n", test_iterations);
	printf("Mode:	%d\n", test_mode);
	printf("Source:	[%p, %p)\n", src, src + test_size);
	printf("Dest:	[%p, %p)\n", dst, dst + test_size);

	for (i = 0; i < test_iterations; i++)
		do_memcpy(dst, src);

	destroy_test_buffer(dst);
	destroy_test_buffer(src);
}

int main(int argc, char **argv)
{
	int c;

	while ((c = getopt(argc, argv, "hs:i:m:")) != -1) {
		switch (c) {
		case 's':
			test_size = strtoul(optarg, NULL, 0);
			if ((test_size % 64 != 0) || (test_size < 128)) {
				pr_help(argv[0]);
				return 1;
			}
			break;
		case 'i':
			test_iterations = strtoul(optarg, NULL, 0);
			break;
		case 'm':
			test_mode = strtoul(optarg, NULL, 0);
			if (test_mode >= NR_MODES) {
				pr_help(argv[0]);
				return 1;
			}
			break;
		case 'h':
			pr_help(argv[0]);
			return 0;
		}
	}

	run_test();
	return 0;
}
